This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Create React App does not provide SASS/SCSS by default. We make some changes to use this pre-processor.
These are the steps-

1. ```npm install sass-loader node-sass --save-dev```

2. ```npm run eject```
  Ejecting a project means that futures updates to create-react-app will not be easy for you to get.   

3. Add following code in config/webpack.config.dev.js

  ```
  {
    test: /\.(scss|sass)$/,
    use: [{
      loader: "style-loader"
    }, {
        loader: "css-loader"
    }, {
        loader: "sass-loader",
        options: {
          include: paths.appSrc
        }
    }]
  }
  ```  

## Application Structure

1. src/views
  This contains views of the application. These are also React components.

2. src/components
  It will contain reusable components of the app.

3. src/actions
  will contain actions for the components. 

4. src/stores   
  will contain stores for the components.

  