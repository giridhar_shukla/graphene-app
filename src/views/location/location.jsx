import React, { Component } from 'react';
import './location.scss';

import SgMaps from '../../components/sg-maps';

class Location extends Component {

  render() {
    return (
      <div id="location" className="main loc-main">
        <div className="loc-sec">
          <div className="loc-head">LOCATION</div>
          <div className="loc-txt">12 Upper St. Martin’s Lane WC2H 9FB, London</div>
        </div>
        <SgMaps className="map-sec"/>
      </div>
    );
  }
}

export default Location;
