import React, { Component } from 'react';
import './dashboard.scss';

class Dashboard extends Component {

  goToSection(sectionName) {
    document.getElementById(sectionName).scrollIntoView();
  }

  render() {
    return (
        <div className="dashboard">
            
            <ul className="navigation">
              <li className="left logo">LOGO</li>
              <li className="left"><a onClick={()=>this.goToSection('dashboard')}>About</a></li>
              <li className="left"><a onClick={()=>this.goToSection('community')}>Community</a></li>
              <li className="left"><a onClick={()=>this.goToSection('location')}>Location</a></li>
              <li className="left"><a onClick={()=>this.goToSection('ourmenu')}>Our Menu</a></li>
              <li className="left"><a onClick={()=>this.goToSection('popularrecipe')}>Recipes</a></li>
              <li className="right"><a>Login</a></li>
              <li className="right"><a onClick={()=>this.goToSection('contactus')}>Contact</a></li>
            </ul>
 
          <div className="logo-recipe">
            <img src="../stamp.png" alt="logo-image"/>
          </div>
          
          <div id="dashboard" className="about">
            <div className="about1">The Best foodie</div>
            <div className="about2">experience</div>
            <div className="about3">Now in London</div>
          </div>
          <div className="request-info">
            Request Info
          </div>  
        </div>
    );
  }
}

export default Dashboard;
