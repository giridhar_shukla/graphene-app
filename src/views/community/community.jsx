import React, { Component } from "react";
import "./community.scss";
import { Grid, Row, Col } from "react-bootstrap";

class Community extends Component {
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    // TODO: Thi should be generic and go in component directory as separate component
    if (window.scrollY >= 450) {
      document.getElementById("div1").classList.add("animate");
      document.getElementById("div2").classList.add("animate");
      document.getElementById("div3").classList.add("animate");
      document.getElementById("div4").classList.add("animate");
    } else {
      document.getElementById("div1").classList.remove("animate");
      document.getElementById("div2").classList.remove("animate");
      document.getElementById("div3").classList.remove("animate");
      document.getElementById("div4").classList.remove("animate");
    }
  };

  render() {
    return (
      <div id="community" className="main-community">
        <h2 style={{ textAlign: "center", paddingBottom: "25px" }}>
          MEET OUR COMMUNITY
        </h2>
        <Grid>
          <Row className="show-grid">
            <Col xs={4} md={3} id="div1" className="parallax">
              <div >
                <div className="community-item">
                  <div className="head">Heading</div>
                  <div className="content">
                    content loren ipsum content loren ipsum content loren ipsum
                    content loren ipsum content loren ipsum content loren ipsum
                  </div>
                </div>
                <div className="community-item">
                  <div className="head">Heading</div>
                  <div className="content">
                    content loren ipsum content loren ipsum content loren ipsum
                    content loren ipsum content loren ipsum content loren ipsum
                  </div>
                </div>
              </div>
            </Col>
            <Col xs={4} md={3} id="div2" className="parallax">
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
            </Col>
            <Col xs={4} md={3} id="div3" className="parallax">
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
            </Col>
            <Col xsHidden md={3} id="div4" className="parallax">
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
              <div className="community-item">
                <div className="head">Heading</div>
                <div className="content">
                  content loren ipsum content loren ipsum content loren ipsum
                  content loren ipsum content loren ipsum content loren ipsum
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Community;
