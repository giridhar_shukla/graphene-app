import React, { Component } from "react";
import "./our-menu.scss";

import { Grid, Row, Col } from "react-bootstrap";

class OurMenu extends Component {
  render() {
    return (
      <div id="ourmenu" className="main-our-menu">
        <Grid>
          <Row className="show-grid">
            <Col className="border-content parallax" xs={4} md={3}>
              <div>STARTERS</div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
            </Col>
            <Col className="border-content parallax" xs={4} md={3}>
              <div>MAIN COURSES</div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
            </Col>
            <Col className="border-content parallax" xs={4} md={3}>
              <div>SIDES</div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
            </Col>
            <Col className="border-content parallax" xsHidden md={3}>
              <div>DESSERTS</div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
              <div className="menu-item">
                <div className="head">Heading</div>
                <div className="content">content loren ipsum</div>
                <div className="price">$54</div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default OurMenu;
