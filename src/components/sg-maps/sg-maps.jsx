import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const GrapheneMap = withScriptjs(withGoogleMap((props) =>
<GoogleMap
  defaultZoom={8}
  defaultCenter={{ lat: 51.5124319, lng: -0.1269096 }}
>
  {props.isMarkerShown && <Marker position={{ lat: 51.5124319, lng: -0.1269096 }} />}
</GoogleMap>));

class SgMaps extends Component {

  render() {
    return (
      <GrapheneMap 
        isMarkerShown
        googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />} 
      />
    );
  }
}

export default SgMaps;
