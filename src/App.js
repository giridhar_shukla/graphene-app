import React, { Component } from 'react';
import './App.scss';

import Dashboard from './views/dashboard';
import Community from './views/community';
import Location from './views/location';
import OurMenu from './views/our-menu';
import PopularRecipe from './views/popular-recipe';
import SgFooter from './views/sg-footer';
import ContactUs from './views/contact-us';

class App extends Component {

  render() {
    return (
      <div>
        <Dashboard />
        <Community />
        <Location />
        {/* <OurMenu /> */}
        {/* <PopularRecipe /> */}
        {/* <ContactUs /> */}
        {/* <SgFooter /> */}
      </div>
    );
  }
}

export default App;
